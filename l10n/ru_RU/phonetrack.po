msgid ""
msgstr ""
"Project-Id-Version: phonetrack\n"
"Report-Msgid-Bugs-To: translations@owncloud.org\n"
"POT-Creation-Date: 2018-03-20 17:56+0100\n"
"PO-Revision-Date: 2018-03-20 13:03-0400\n"
"Last-Translator: eneiluj <eneiluj@posteo.net>\n"
"Language-Team: Russian\n"
"Language: ru_RU\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=((n%10==1 && n%100!=11) ? 0 : ((n%10 >= 2 && n%10 <=4 && (n%100 < 12 || n%100 > 14)) ? 1 : ((n%10 == 0 || (n%10 >= 5 && n%10 <=9)) || (n%100 >= 11 && n%100 <= 14)) ? 2 : 3));\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: phonetrack\n"
"X-Crowdin-Language: ru\n"
"X-Crowdin-File: /master/l10n/templates/phonetrack.pot\n"

#: app.php:41
msgid "PhoneTrack"
msgstr "PhoneTrack"

#: logcontroller.php:200 logcontroller.php:223
msgid "Geofencing alert"
msgstr ""

#: logcontroller.php:203
#, php-format
msgid "In session \"%s\", device \"%s\" entered geofencing zone \"%s\"."
msgstr ""

#: logcontroller.php:226
#, php-format
msgid "In session \"%s\", device \"%s\" exited geofencing zone \"%s\"."
msgstr ""

#: leaflet.js:5
msgid "left"
msgstr "влево"

#: leaflet.js:5
msgid "right"
msgstr "вправо"

#: phonetrack.js:544 maincontent.php:69
msgid "Show lines"
msgstr "Показать линии"

#: phonetrack.js:552
msgid "Hide lines"
msgstr "Скрыть линии"

#: phonetrack.js:573
msgid "Activate automatic zoom"
msgstr "Активировать автоматическое масштабирование"

#: phonetrack.js:581
msgid "Disable automatic zoom"
msgstr "Отключить автоматическое масштабирование"

#: phonetrack.js:602
msgid "Show last point tooltip"
msgstr "Показать последнюю точку подсказки"

#: phonetrack.js:610
msgid "Hide last point tooltip"
msgstr "Скрыть последнюю точку подсказки"

#: phonetrack.js:631
msgid "Zoom on all devices"
msgstr "Увеличить на всех устройствах"

#: phonetrack.js:644
msgid "Click on the map to move the point, press ESC to cancel"
msgstr "Нажмите на карту, чтобы переместить точку, нажмите клавишу ESC, чтобы отменить"

#: phonetrack.js:847
msgid "Server name or server url should not be empty"
msgstr "Имя и адрес сервера не могут быть пустыми"

#: phonetrack.js:850 phonetrack.js:859
msgid "Impossible to add tile server"
msgstr "Невозможно добавить тайловый сервер"

#: phonetrack.js:856
msgid "A server with this name already exists"
msgstr "Сервер с таким именем уже существует"

#: phonetrack.js:892 phonetrack.js:3194 maincontent.php:236 maincontent.php:280
#: maincontent.php:325 maincontent.php:374
msgid "Delete"
msgstr "Удалить"

#: phonetrack.js:925
msgid "Tile server \"{ts}\" has been added"
msgstr "Тайловый сервер \"{ts}\" добавлен"

#: phonetrack.js:928
msgid "Failed to add tile server \"{ts}\""
msgstr "Ошибка добавления тайлового сервера \"{ts}\""

#: phonetrack.js:932
msgid "Failed to contact server to add tile server"
msgstr "Не удалось связаться с сервером для добавления тайлового сервера"

#: phonetrack.js:966
msgid "Tile server \"{ts}\" has been deleted"
msgstr "Тайловый сервер \"{ts}\" удалён"

#: phonetrack.js:969
msgid "Failed to delete tile server \"{ts}\""
msgstr "Ошибка удаления сервера тайлов \"{ts}\""

#: phonetrack.js:973
msgid "Failed to contact server to delete tile server"
msgstr "Не удалось связаться с сервером для удаления тайлового сервера"

#: phonetrack.js:1098
msgid "Failed to contact server to restore options values"
msgstr "Не удалось связаться с сервером для восстановления настроек"

#: phonetrack.js:1101 phonetrack.js:1181 phonetrack.js:4668
msgid "Reload this page"
msgstr "Обновить эту страницу"

#: phonetrack.js:1178
msgid "Failed to contact server to save options values"
msgstr "Не удалось связаться с сервером для сохранения настроек"

#: phonetrack.js:1201
msgid "Session name should not be empty"
msgstr "Имя сессии не может быть пустым"

#: phonetrack.js:1218
msgid "Session name already used"
msgstr "Имя сессии уже используется"

#: phonetrack.js:1222
msgid "Failed to contact server to create session"
msgstr "Не удалось связаться с сервером для создания сессии"

#: phonetrack.js:1317
msgid "Watch this session"
msgstr "Посмотреть эту сессию"

#: phonetrack.js:1323
msgid "shared by {u}"
msgstr "поделиться с {u}"

#: phonetrack.js:1328 phonetrack.js:2397
msgid "More actions"
msgstr "Еще действия"

#: phonetrack.js:1332
msgid "Zoom on this session"
msgstr "Увеличить на этой сессии"

#: phonetrack.js:1335
msgid "URL to share session"
msgstr "URL-адрес, чтобы поделиться сессией"

#: phonetrack.js:1339
msgid "URLs for logging apps"
msgstr "URL-адреса для журнала приложений"

#: phonetrack.js:1343
msgid "Reserve device names"
msgstr "Резервное имя устройства"

#: phonetrack.js:1352
msgid "Delete session"
msgstr "Удалить сессию"

#: phonetrack.js:1353 phonetrack.js:1354
msgid "Rename session"
msgstr "Переименовать сессию"

#: phonetrack.js:1356 phonetrack.js:1357
msgid "Export to gpx"
msgstr "Экспорт в gpx"

#: phonetrack.js:1361
msgid "Files are created in '{exdir}'"
msgstr "Файлы созданы в '{exdir}'"

#: phonetrack.js:1362
msgid "Automatic export"
msgstr "Автоматический экспорт"

#: phonetrack.js:1364
msgid "never"
msgstr "никогда"

#: phonetrack.js:1365
msgid "daily"
msgstr "ежедневно"

#: phonetrack.js:1366
msgid "weekly"
msgstr "еженедельно"

#: phonetrack.js:1367
msgid "monthly"
msgstr "ежемесячно"

#: phonetrack.js:1372
msgid "Automatic purge of points older than"
msgstr ""

#: phonetrack.js:1374
msgid "no purge"
msgstr ""

#: phonetrack.js:1375
msgid "a day"
msgstr ""

#: phonetrack.js:1376
msgid "a week"
msgstr ""

#: phonetrack.js:1377
msgid "a month"
msgstr ""

#: phonetrack.js:1386
msgid "Name reservation is optional."
msgstr "Резервирование имени является необязательным."

#: phonetrack.js:1387
msgid "Name can be set directly in logging URL if it is not reserved."
msgstr "Имя можно задать непосредственно в адресе URL журнала, если оно не зарезервировано."

#: phonetrack.js:1388
msgid "To log with a reserved name, use its token in logging URL."
msgstr "Для входа с зарезервированным именем, используйте свой маркер в адресе URL журнала."

#: phonetrack.js:1389
msgid "If a name is reserved, the only way to log with this name is with its token."
msgstr "Если имя зарезервировано, единственный способ для входа с этим именем связана с ее маркером."

#: phonetrack.js:1392
msgid "Reserve this device name"
msgstr "Зарезервировать это имя устройства"

#: phonetrack.js:1394
msgid "Type reserved name and press 'Enter'"
msgstr "Введите зарезервированное имя и нажмите «Enter»"

#: phonetrack.js:1408
msgid "Share with user"
msgstr "Поделиться с пользователем"

#: phonetrack.js:1410
msgid "Type user name and press 'Enter'"
msgstr "Введите имя пользователя и нажмите «Enter»"

#: phonetrack.js:1415 phonetrack.js:3732
msgid "Shared with {u}"
msgstr "Поделиться с {u}"

#: phonetrack.js:1421
msgid "A private session is not visible on public browser logging page"
msgstr "Частные сессии не видны на публичных страницах логов"

#: phonetrack.js:1423 phonetrack.js:4677
msgid "Make session public"
msgstr "Сделать сессию публичной"

#: phonetrack.js:1426 phonetrack.js:4672
msgid "Make session private"
msgstr "Сделать сессию приватной"

#: phonetrack.js:1431
msgid "Public watch URL"
msgstr "Ссылки на публичные логи"

#: phonetrack.js:1433
msgid "API URL (JSON last positions)"
msgstr "API URL (JSON последняя позиция)"

#: phonetrack.js:1439
msgid "Current active filters will be applied on shared view"
msgstr "Текущие активные фильтры будут применяться на общем представлении"

#: phonetrack.js:1441
msgid "Add public filtered share"
msgstr "Добавить общественных отфильтрованных ссылок"

#: phonetrack.js:1451
msgid "List of server URLs to configure logging apps."
msgstr "Список URL-адресов сервера для настройки ведения журнала приложений."

#: phonetrack.js:1452 maincontent.php:168
msgid "Replace 'yourname' with the desired device name or with the name reservation token"
msgstr "Замените «ВашеИмя» на желаемое имя устройства или маркер бронирования"

#: phonetrack.js:1454
msgid "Public browser logging URL"
msgstr "Адрес URL журнала общественного браузера"

#: phonetrack.js:1456
msgid "OsmAnd URL"
msgstr "URL-адрес приложения OsmAnd"

#: phonetrack.js:1460
msgid "GpsLogger GET and POST URL"
msgstr "GpsLogger GET и POST URL"

#: phonetrack.js:1464
msgid "Owntracks (HTTP mode) URL"
msgstr "URL-адрес Owntracks (режим HTTP)"

#: phonetrack.js:1468
msgid "Ulogger URL"
msgstr "URL-адрес Ulogger"

#: phonetrack.js:1472
msgid "Traccar URL"
msgstr "URL-адрес Traccar"

#: phonetrack.js:1476
msgid "OpenGTS URL"
msgstr "URL-адрес OpenGTS"

#: phonetrack.js:1479
msgid "HTTP GET URL"
msgstr "HTTP GET URL"

#: phonetrack.js:1526
msgid "The session you want to delete does not exist"
msgstr "Сессия, которую вы хотите удалить не существует"

#: phonetrack.js:1529
msgid "Failed to delete session"
msgstr "Не удалось удалить сессию"

#: phonetrack.js:1533
msgid "Failed to contact server to delete session"
msgstr "Не удалось связаться с сервером, чтобы удалить сессии"

#: phonetrack.js:1553
msgid "Device '{d}' of session '{s}' has been deleted"
msgstr "Устройство '{d}' сессии '{s}' был удален"

#: phonetrack.js:1556
msgid "Failed to delete device '{d}' of session '{s}'"
msgstr "Не удалось удалить устройство '{d}' сессии '{s}»"

#: phonetrack.js:1560
msgid "Failed to contact server to delete device"
msgstr "Не удалось связаться с сервером для удаления устройства"

#: phonetrack.js:1612
msgid "Impossible to rename session"
msgstr "Невозможно переименовать сессию"

#: phonetrack.js:1616
msgid "Failed to contact server to rename session"
msgstr "Не удалось связаться с сервером, чтобы переименовать сессию"

#: phonetrack.js:1664
msgid "Impossible to rename device"
msgstr "Невозможно переименовать устройство"

#: phonetrack.js:1668
msgid "Failed to contact server to rename device"
msgstr "Не удалось связаться с сервером, чтобы переименовать устройство"

#: phonetrack.js:1725
msgid "Device already exists in target session"
msgstr "Устройство уже существует в целевом сессии"

#: phonetrack.js:1728
msgid "Impossible to move device to another session"
msgstr "Невозможно переместить устройство в другой сеанс"

#: phonetrack.js:1732
msgid "Failed to contact server to move device"
msgstr "Не удалось связаться с сервером для перемещения устройства"

#: phonetrack.js:1799
msgid "Failed to contact server to get sessions"
msgstr "Не удалось связаться с сервером для получения сеансов"

#: phonetrack.js:2109 phonetrack.js:2138
msgid "Stats of all points"
msgstr "Статистика всех точек"

#: phonetrack.js:2135
msgid "Stats of filtered points"
msgstr "Статистика отфильтрованных точек"

#: phonetrack.js:2318
msgid "Device's color successfully changed"
msgstr "Цвет устройства успешно изменен"

#: phonetrack.js:2321
msgid "Failed to save device's color"
msgstr "Не удалось сохранить цвет устройства"

#: phonetrack.js:2325
msgid "Failed to contact server to change device's color"
msgstr "Не удалось связаться с сервером, чтобы изменить цвет устройства"

#: phonetrack.js:2400
msgid "Delete this device"
msgstr "Удалить это устройство"

#: phonetrack.js:2402
msgid "Rename this device"
msgstr "Переименовать это устройство"

#: phonetrack.js:2405
msgid "Move to another session"
msgstr "Перейти к другому сеансу"

#: phonetrack.js:2408 maincontent.php:48
msgid "Ok"
msgstr "Ок"

#: phonetrack.js:2416
msgid "Device geofencing zones"
msgstr ""

#: phonetrack.js:2420
msgid "Use current map view as geofencing zone"
msgstr ""

#: phonetrack.js:2421
msgid "Add zone"
msgstr ""

#: phonetrack.js:2432
msgid "Toggle detail/edition points"
msgstr "Переключение точки детали/издание"

#: phonetrack.js:2440
msgid "Toggle lines"
msgstr "Показать линии сетки"

#: phonetrack.js:2451 phonetrack.js:2457
msgid "Center map on device"
msgstr "Центрировать карту на устройстве"

#: phonetrack.js:2463
msgid "Follow this device (autozoom)"
msgstr "Следовать за это устройством (Автомасштабирование)"

#: phonetrack.js:2827
msgid "The point you want to edit does not exist or you're not allowed to edit it"
msgstr "Точка, которую вы хотите отредактировать не существует, или у вас нет прав на изменение"

#: phonetrack.js:2831
msgid "Failed to contact server to edit point"
msgstr "Не удалось связаться с сервером для редактирования точки"

#: phonetrack.js:2940
msgid "The point you want to delete does not exist or you're not allowed to delete it"
msgstr "Точка, которую вы хотите удалить не существует, или вы не можете удалить ее"

#: phonetrack.js:2944
msgid "Failed to contact server to delete point"
msgstr "Не удалось связаться с сервером, чтобы удалить точку"

#: phonetrack.js:3038
msgid "Impossible to add this point"
msgstr "Невозможно добавить эту точку"

#: phonetrack.js:3042
msgid "Failed to contact server to add point"
msgstr "Не удалось связаться с сервером, чтобы добавить точку"

#: phonetrack.js:3150
msgid "Date"
msgstr "Дата"

#: phonetrack.js:3153
msgid "Time"
msgstr "Время"

#: phonetrack.js:3158 phonetrack.js:3210
msgid "Altitude"
msgstr "Высота"

#: phonetrack.js:3161 phonetrack.js:3214
msgid "Precision"
msgstr "Точность"

#: phonetrack.js:3164 phonetrack.js:3220
msgid "Speed"
msgstr ""

#: phonetrack.js:3172 phonetrack.js:3224
msgid "Bearing"
msgstr ""

#: phonetrack.js:3175 phonetrack.js:3228
msgid "Satellites"
msgstr "Спутники"

#: phonetrack.js:3178 phonetrack.js:3232
msgid "Battery"
msgstr "Батарея"

#: phonetrack.js:3181 phonetrack.js:3236
msgid "User-agent"
msgstr "Агент пользователя"

#: phonetrack.js:3184
msgid "lat : lng"
msgstr "широта : долгота"

#: phonetrack.js:3188
msgid "DMS coords"
msgstr "DMS coords"

#: phonetrack.js:3193
msgid "Save"
msgstr "Сохранить"

#: phonetrack.js:3195
msgid "Move"
msgstr "Переместить"

#: phonetrack.js:3196
msgid "Cancel"
msgstr "Отмена"

#: phonetrack.js:3410
msgid "File extension must be '.gpx' to be imported"
msgstr "Расширение файла должно быть «.gpx» для импорта"

#: phonetrack.js:3428
msgid "Failed to create imported session"
msgstr "Не удается создать импортированные сессии"

#: phonetrack.js:3432 phonetrack.js:3438
msgid "Failed to import session"
msgstr "Не удалось импортировать сессии"

#: phonetrack.js:3433
msgid "File is not readable"
msgstr "Файл не читается"

#: phonetrack.js:3439
msgid "File does not exist"
msgstr "Файл не существует"

#: phonetrack.js:3447
msgid "Failed to contact server to import session"
msgstr "Не удалось связаться с сервером для импорта сессии"

#: phonetrack.js:3467
msgid "Session successfully exported in"
msgstr "Сессия успешно экспортированна в"

#: phonetrack.js:3471
msgid "Failed to export session"
msgstr "Не удалось экспортировать сессию"

#: phonetrack.js:3476
msgid "Failed to contact server to export session"
msgstr "Не удалось связаться с сервером для экспорта сессии"

#: phonetrack.js:3507
msgid "Failed to contact server to log position"
msgstr "Не удалось связаться с сервером для записи позиции"

#: phonetrack.js:3655
msgid "'{n}' is already reserved"
msgstr "'{n}' уже зарезервировано"

#: phonetrack.js:3658
msgid "Failed to reserve '{n}'"
msgstr "Не удалось зарезервировать '{n}»"

#: phonetrack.js:3661
msgid "Failed to contact server to reserve device name"
msgstr "Не удалось связаться с сервером, чтобы зарезервировать имя устройства"

#: phonetrack.js:3692 phonetrack.js:3696
msgid "Failed to delete reserved name"
msgstr "Не удалось удалить зарезервированное имя"

#: phonetrack.js:3693
msgid "This device does not exist"
msgstr "Это устройство не существует"

#: phonetrack.js:3697
msgid "This device name is not reserved, please reload this page"
msgstr "Это имя устройства не зарезервировано, пожалуйста обновите эту страницу"

#: phonetrack.js:3700
msgid "Failed to contact server to delete reserved name"
msgstr "Не удалось связаться с сервером, чтобы удалить зарезервированное имя"

#: phonetrack.js:3720
msgid "User does not exist"
msgstr "Пользователь не существует"

#: phonetrack.js:3723
msgid "Failed to add user share"
msgstr "Не удалось добавить доступ пользователю"

#: phonetrack.js:3726
msgid "Failed to contact server to add user share"
msgstr "Не удалось связаться с сервером для добавления доступа пользователю"

#: phonetrack.js:3757
msgid "Failed to delete user share"
msgstr "Не удалось удалить доступ пользователю"

#: phonetrack.js:3760
msgid "Failed to contact server to delete user share"
msgstr "Не удалось связаться с сервером для удаления доступа пользователю"

#: phonetrack.js:3778 phonetrack.js:3802
msgid "Public share has been successfully modified"
msgstr ""

#: phonetrack.js:3781 phonetrack.js:3805
msgid "Failed to modify public share"
msgstr ""

#: phonetrack.js:3784 phonetrack.js:3808
msgid "Failed to contact server to modify public share"
msgstr ""

#: phonetrack.js:3826
msgid "Device name restriction has been successfully set"
msgstr ""

#: phonetrack.js:3829
msgid "Failed to set public share device name restriction"
msgstr ""

#: phonetrack.js:3832
msgid "Failed to contact server to set public share device name restriction"
msgstr ""

#: phonetrack.js:3860
msgid "Warning : User email and server admin email must be set to receive geofencing alerts."
msgstr ""

#: phonetrack.js:3864
msgid "Failed to add geofencing zone"
msgstr ""

#: phonetrack.js:3867
msgid "Failed to contact server to add geofencing zone"
msgstr ""

#: phonetrack.js:3901
msgid "Failed to delete geofencing zone"
msgstr ""

#: phonetrack.js:3904
msgid "Failed to contact server to delete geofencing zone"
msgstr ""

#: phonetrack.js:3923
msgid "Failed to add public share"
msgstr "Не удалось добавить общий ресурс"

#: phonetrack.js:3926
msgid "Failed to contact server to add public share"
msgstr "Не удалось связаться с сервером, чтобы добавить общий ресурс"

#: phonetrack.js:3945
msgid "Show this device only"
msgstr ""

#: phonetrack.js:3947
msgid "Show last positions only"
msgstr ""

#: phonetrack.js:3949
msgid "Simplify positions to nearest geofencing zone center"
msgstr ""

#: phonetrack.js:3968
msgid "No filters"
msgstr "Без фильтров"

#: phonetrack.js:3992
msgid "Failed to delete public share"
msgstr "Не удалось удалить общий ресурс"

#: phonetrack.js:3995
msgid "Failed to contact server to delete public share"
msgstr "Не удалось связаться с сервером, чтобы удалить общий ресурс"

#: phonetrack.js:4013
msgid "Failed to contact server to get user list"
msgstr "Не удалось связаться с сервером, чтобы получить список пользователей"

#: phonetrack.js:4026
msgid "device name"
msgstr "имя устройства"

#: phonetrack.js:4027
msgid "distance (km)"
msgstr "Pасстояние (км)"

#: phonetrack.js:4028
msgid "duration"
msgstr "Продолжительность"

#: phonetrack.js:4029
msgid "#points"
msgstr "#points"

#: phonetrack.js:4064
msgid "years"
msgstr "лет"

#: phonetrack.js:4067
msgid "days"
msgstr "дней"

#: phonetrack.js:4086
msgid "In OsmAnd, go to 'Plugins' in the main menu, then activate 'Trip recording' plugin and go to its settings."
msgstr "В приложения OsmAnd перейдите на «Плагины» в главном меню, затем активируйте плагин «Запись Путешествия» и перейдите в его параметры."

#: phonetrack.js:4087
msgid "Copy the URL below into the 'Online tracking web address' field."
msgstr "Скопируйте URL расположенный ниже в поле «Онлайн отслеживание веб-адреса»."

#: phonetrack.js:4091
msgid "In GpsLogger, go to 'Logging details' in the sidebar menu, then activate 'Log to custom URL'."
msgstr "В GpsLogger перейдите на «Вход подробности» в меню боковой панели, а затем активировать «Log для пользовательского URL»."

#: phonetrack.js:4092
msgid "Copy the URL below into the 'URL' field."
msgstr "Скопируйте URL ниже в поле «URL»."

#: phonetrack.js:4100
msgid "In Ulogger, go to settings menu and copy the URL below into the 'Server URL' field."
msgstr "В Ulogger перейдите в меню настройки и скопируйте URL-адрес ниже в поле «URL-адрес сервера»."

#: phonetrack.js:4101
msgid "Set 'User name' and 'Password' mandatory fields to any value as they will be ignored by PhoneTrack."
msgstr "Установите «Имя пользователя» и «Пароль» обязательные поля в любое значение, так как они будут игнорироваться PhoneTrack."

#: phonetrack.js:4102
msgid "Activate 'Live synchronization'."
msgstr "Активируйте «Live синхронизация»."

#: phonetrack.js:4106
msgid "In Traccar client, copy the URL below into the 'server URL' field."
msgstr "В Traccar клиенте скопируйте URL ниже в поле «URL-адрес сервера»."

#: phonetrack.js:4110
msgid "You can log with any other client with a simple HTTP request."
msgstr "Вы можете войти с любым другим клиентом, с простым HTTP-запросом."

#: phonetrack.js:4111
msgid "Make sure the logging system sets values for at least 'timestamp', 'lat' and 'lon' GET parameters."
msgstr "Убедитесь, что система протоколирования, устанавливает значения для по крайней мере 'timestamp', 'lat' и 'lon' GET parameters"

#: phonetrack.js:4114
msgid "Configure {loggingApp} for logging to session '{sessionName}'"
msgstr "Настройка {loggingApp} для ведения журнала сессии '{sessionName}»"

#: phonetrack.js:4245
msgid "Are you sure you want to delete the session {session} ?"
msgstr "Вы уверены, что хотите удалить эту сессию {session} ?"

#: phonetrack.js:4248
msgid "Confirm session deletion"
msgstr "Подтверждение удаления сессии"

#: phonetrack.js:4306
msgid "Choose auto export target path"
msgstr "Выберите путь авто экспорта"

#: phonetrack.js:4436
msgid "Select storage location for '{fname}'"
msgstr "Выберите путь хранения для '{fname}'"

#: phonetrack.js:4603
msgid "Are you sure you want to delete the device {device} ?"
msgstr "Вы уверены, что хотите удалить это устройство {device} ?"

#: phonetrack.js:4606
msgid "Confirm device deletion"
msgstr "Подтверждение удаления устройства"

#: phonetrack.js:4664
msgid "Failed to toggle session public status, session does not exist"
msgstr "Не удалось изменить статус сессии, сессия не существует"

#: phonetrack.js:4667
msgid "Failed to contact server to toggle session public status"
msgstr ""

#: phonetrack.js:4700
msgid "Failed to set session auto export value"
msgstr ""

#: phonetrack.js:4701 phonetrack.js:4728
msgid "session does not exist"
msgstr ""

#: phonetrack.js:4705
msgid "Failed to contact server to set session auto export value"
msgstr ""

#: phonetrack.js:4727
msgid "Failed to set session auto purge value"
msgstr ""

#: phonetrack.js:4732
msgid "Failed to contact server to set session auto purge value"
msgstr ""

#: phonetrack.js:4810
msgid "Import gpx session file"
msgstr ""

#: maincontent.php:4
msgid "Main tab"
msgstr ""

#: maincontent.php:5
msgid "Filters"
msgstr ""

#: maincontent.php:14
msgid "Stats"
msgstr ""

#: maincontent.php:15 maincontent.php:201
msgid "Settings and extra actions"
msgstr ""

#: maincontent.php:16 maincontent.php:595
msgid "About PhoneTrack"
msgstr ""

#: maincontent.php:36
msgid "Import session"
msgstr ""

#: maincontent.php:40
msgid "Create session"
msgstr ""

#: maincontent.php:44
msgid "Session name"
msgstr ""

#: maincontent.php:57
msgid "Options"
msgstr ""

#: maincontent.php:65
msgid "Auto zoom"
msgstr ""

#: maincontent.php:73
msgid "Show tooltips"
msgstr ""

#: maincontent.php:76
msgid "Refresh each (sec)"
msgstr ""

#: maincontent.php:79
msgid "Refresh"
msgstr ""

#: maincontent.php:83
msgid "Show accuracy in tooltips"
msgstr ""

#: maincontent.php:87
msgid "Show speed in tooltips"
msgstr ""

#: maincontent.php:91
msgid "Show bearing in tooltips"
msgstr ""

#: maincontent.php:95
msgid "Show satellites in tooltips"
msgstr ""

#: maincontent.php:99
msgid "Show battery level in tooltips"
msgstr ""

#: maincontent.php:103
msgid "Show elevation in tooltips"
msgstr ""

#: maincontent.php:107
msgid "Show user-agent in tooltips"
msgstr ""

#: maincontent.php:112
msgid "Make points draggable in edition mode"
msgstr ""

#: maincontent.php:116
msgid "Show accuracy circle on hover"
msgstr ""

#: maincontent.php:119
msgid "Line width"
msgstr ""

#: maincontent.php:123
msgid "Point radius"
msgstr ""

#: maincontent.php:127
msgid "Points and lines opacity"
msgstr ""

#: maincontent.php:131
msgid "Theme"
msgstr ""

#: maincontent.php:133
msgid "bright"
msgstr ""

#: maincontent.php:134
msgid "pastel"
msgstr ""

#: maincontent.php:135
msgid "dark"
msgstr ""

#: maincontent.php:139
msgid "Auto export path"
msgstr ""

#: maincontent.php:144
msgid "Export one file per device"
msgstr ""

#: maincontent.php:146
msgid "reload page to make changes effective"
msgstr ""

#: maincontent.php:153
msgid "Device name"
msgstr ""

#: maincontent.php:155
msgid "Log my position in this session"
msgstr ""

#: maincontent.php:160
msgid "Tracking sessions"
msgstr ""

#: maincontent.php:206
msgid "Custom tile servers"
msgstr ""

#: maincontent.php:209 maincontent.php:249 maincontent.php:292
#: maincontent.php:337
msgid "Server name"
msgstr ""

#: maincontent.php:210 maincontent.php:250 maincontent.php:293
#: maincontent.php:338
msgid "For example : my custom server"
msgstr ""

#: maincontent.php:211 maincontent.php:251 maincontent.php:294
#: maincontent.php:339
msgid "Server url"
msgstr ""

#: maincontent.php:212 maincontent.php:295
msgid "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png"
msgstr ""

#: maincontent.php:213 maincontent.php:253 maincontent.php:296
#: maincontent.php:341
msgid "Min zoom (1-20)"
msgstr ""

#: maincontent.php:215 maincontent.php:255 maincontent.php:298
#: maincontent.php:343
msgid "Max zoom (1-20)"
msgstr ""

#: maincontent.php:217 maincontent.php:261 maincontent.php:306
#: maincontent.php:355
msgid "Add"
msgstr ""

#: maincontent.php:220
msgid "Your tile servers"
msgstr ""

#: maincontent.php:246
msgid "Custom overlay tile servers"
msgstr ""

#: maincontent.php:252 maincontent.php:340
msgid "For example : http://overlay.server.org/cycle/{z}/{x}/{y}.png"
msgstr ""

#: maincontent.php:257 maincontent.php:345
msgid "Transparent"
msgstr ""

#: maincontent.php:259 maincontent.php:347
msgid "Opacity (0.0-1.0)"
msgstr "Прозрачность (0.0-1.0)"

#: maincontent.php:264
msgid "Your overlay tile servers"
msgstr "Ваши серверы плитка оверлея"

#: maincontent.php:289
msgid "Custom WMS tile servers"
msgstr "Пользовательские плитки для сервера WMS"

#: maincontent.php:300 maincontent.php:349
msgid "Format"
msgstr "Формат"

#: maincontent.php:302 maincontent.php:351
msgid "WMS version"
msgstr "WMS версии"

#: maincontent.php:304 maincontent.php:353
msgid "Layers to display"
msgstr "Слои для отображения"

#: maincontent.php:309
msgid "Your WMS tile servers"
msgstr "Ваш WMS плитка серверов"

#: maincontent.php:334
msgid "Custom WMS overlay servers"
msgstr "Пользовательские WMS наложение серверов"

#: maincontent.php:358
msgid "Your WMS overlay tile servers"
msgstr ""

#: maincontent.php:387
msgid "Manually add a point"
msgstr ""

#: maincontent.php:389 maincontent.php:407
msgid "Session"
msgstr ""

#: maincontent.php:393 maincontent.php:411
msgid "Device"
msgstr ""

#: maincontent.php:395
msgid "Add a point"
msgstr ""

#: maincontent.php:396
msgid "Now, click on the map to add a point (if session is not activated, you won't see added point)"
msgstr ""

#: maincontent.php:397
msgid "Cancel add point"
msgstr ""

#: maincontent.php:402
msgid "Delete multiple points"
msgstr ""

#: maincontent.php:405
msgid "Choose a session, a device and adjust the filters. All displayed points for selected device will be deleted. An empty device name selects them all."
msgstr ""

#: maincontent.php:413
msgid "Delete points"
msgstr ""

#: maincontent.php:414
msgid "Delete only visible points"
msgstr ""

#: maincontent.php:421
msgid "Filter points"
msgstr ""

#: maincontent.php:426
msgid "Apply filters"
msgstr ""

#: maincontent.php:430
msgid "Begin date"
msgstr ""

#: maincontent.php:436 maincontent.php:455
msgid "today"
msgstr ""

#: maincontent.php:442
msgid "Begin time"
msgstr ""

#: maincontent.php:449
msgid "End date"
msgstr ""

#: maincontent.php:461
msgid "End time"
msgstr ""

#: maincontent.php:468
msgid "Min-- and Max--"
msgstr ""

#: maincontent.php:471
msgid "Min++ and Max++"
msgstr ""

#: maincontent.php:475
msgid "Last day:hour:min"
msgstr ""

#: maincontent.php:483
msgid "Minimum accuracy"
msgstr ""

#: maincontent.php:491
msgid "Maximum accuracy"
msgstr ""

#: maincontent.php:499
msgid "Minimum elevation"
msgstr ""

#: maincontent.php:507
msgid "Maximum elevation"
msgstr ""

#: maincontent.php:515
msgid "Minimum battery level"
msgstr ""

#: maincontent.php:523
msgid "Maximum battery level"
msgstr ""

#: maincontent.php:531
msgid "Minimum speed"
msgstr ""

#: maincontent.php:539
msgid "Maximum speed"
msgstr ""

#: maincontent.php:547
msgid "Minimum bearing"
msgstr ""

#: maincontent.php:555
msgid "Maximum bearing"
msgstr ""

#: maincontent.php:563
msgid "Minimum satellites"
msgstr ""

#: maincontent.php:571
msgid "Maximum satellites"
msgstr ""

#: maincontent.php:583
msgid "Statistics"
msgstr ""

#: maincontent.php:588
msgid "Show stats"
msgstr ""

#: maincontent.php:597
msgid "Shortcuts"
msgstr ""

#: maincontent.php:599
msgid "Toggle sidebar"
msgstr ""

#: maincontent.php:603
msgid "Documentation"
msgstr ""

#: maincontent.php:611
msgid "Source management"
msgstr ""

#: maincontent.php:625
msgid "Authors"
msgstr "Авторы"

